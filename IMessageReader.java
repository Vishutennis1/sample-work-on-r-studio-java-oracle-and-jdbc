package com.mtc.message;

public interface IMessageReader {

	String fetchMessage();
}
