rm(list=ls())
library(tidyverse)
diamonds<- read.csv("C:\Users\zanza\Desktop\R-school")
names(diamonds)
head(diamonds)
str(diamonds)
diamonds$region <- as.factor(diamonds$region)
str(diamonds)
summary(diamonds)
set.seed(113)
val.ind<-sample(1:nrow(diamonds),ceiling(0.10*nrow(diamonds)),replace=FALSE)
val.ind
validation<-diamonds[val.ind,]
tempData <- diamonds [-val.ind,]
nrow(tempData) + nrow(validation) == nrow(diamonds)
train.ind <- sample(1:nrow(tempData),floor(0.8*nrow(tempData)),replace = FALSE)
train <- tempData[train.ind, ]
test <- tempData[-train.ind, ]
mean(train$carat)
mean(c(0.798102,0.7966,0.7972,0.7956))
sd(c(0.798102,0.7966,0.7972,0.7956))
mean(test$carat)
mean(diamonds$carat)
mean(diamonds$carat[sample(1:nrow(diamonds),20,replace = FALSE)])
mean(train$carat) + 2*sd(train$carat)/sqrt(nrow(train))
mean(train$carat) - 2*sd(train$carat)/sqrt(nrow(train))
mpg
ggplot(data = train) + geom_boxplot(mapping = aes(x = color, y = carat))

ggplot(data = test) + geom_boxplot(mapping = aes(x = color, y = carat))


